package ru.pflb.autotests.calculator;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestCalcOther {

    private Calc calc;

    @DataProvider(name = "dataForAddition")
    public Object[][] dataProvider(){
        return new Object[][]{
                {3,3,6},
                {2,3,5},
                {5,6,11}
        };
    }

    @Test(dataProvider = "dataForAddition")
    public void testAddParametrized(int a, int b, int expected){
        int result = calc.add(a,b);
        Assert.assertEquals(result, expected);
    }

    @BeforeClass
    public void setUp(){
        calc = new Calc();
    }
    @Test
    public void testAdd(){
        int result = calc.add(2,3);
        Assert.assertEquals(result, 5);
    }
    @Test
    public void testSubtract(){
        int result = calc.subtract(2,3);
        Assert.assertEquals(result, -1);
    }
    @Test
    public void testDivide(){
        int result = calc.divide(3,3);
        Assert.assertEquals(result, 1);
    }
    @Test
    public void testMulitply(){
        int result = calc.multiply(2,3);
        Assert.assertEquals(result, 6);
    }
}
